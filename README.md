Docker Dart
===========

Simple wrapper around the dart docker image.
Allows access to the latest version of dart.

Installation
------------

This uses antigen for installation:

    antigen bundle https://gitlab.com/matthewfranglen/docker-dart

This makes the `dart` and `pub` commands available.

Make sure that the `~/.pub-cache` folder exists and is owned by the current user.

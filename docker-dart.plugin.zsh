export PATH=${PATH}:$(dirname $0)/bin

if [ ! -e "${HOME}/.pub-cache" ]; then
    mkdir "${HOME}/.pub-cache"
fi
